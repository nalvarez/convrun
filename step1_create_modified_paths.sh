#!/bin/bash
#$1 = full path to svn repo
time svn log --xml -v file://$1  | (read; echo '<?xml version="1.0" encoding="latin1"?>'; cat) | python parse-svnlog.py
