CREATE TABLE modified_paths (
    revision integer NOT NULL,
    path VARCHAR(512) NOT NULL,
    action CHAR(1) NOT NULL
);

CREATE TABLE unique_paths (
    path VARCHAR(512) NOT NULL,
    PRIMARY KEY (path)
);

CREATE TABLE cache_entry (
    regex VARCHAR(512) NOT NULL,
    PRIMARY KEY (regex)
);

CREATE TABLE cache_revs (
    regex VARCHAR(512) NOT NULL REFERENCES cache_entry(regex),
    revision INTEGER NOT NULL
);

CREATE INDEX path_in_mp ON modified_paths (path);
CREATE INDEX revision_nums ON modified_paths (revision);
CREATE INDEX cache_regexes ON cache_revs (regex);

-- there are more indexes, declared implicitly by PRIMARY KEY and REFERENCES
