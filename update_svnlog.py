#!/usr/bin/python
# -*- coding: utf-8 -*-

def get_last_rev(cursor):
    cursor.execute("SELECT MAX(revision) FROM modified_paths")
    result = cursor.fetchone()
    return int(result[0])

def main():
    import psycopg2
    import sys
    import svnlogparser
    import subprocess

    if len(sys.argv) < 3:
        sys.stderr.write("usage: update_svnlog <database> <SVN URL>\n")
        sys.exit(1)

    prog, dbname, svnurl = sys.argv

    conn = psycopg2.connect(database=dbname)
    cursor = conn.cursor()
    last_rev = get_last_rev(cursor)
    print "Fetching log after r%d" % last_rev

    process = subprocess.Popen(['svn', 'log', '-v', '--xml', '-r%d:HEAD' % (last_rev+1), svnurl],
              stdout=subprocess.PIPE)

    xmlfile = process.stdout

    def progress(rev):
        sys.stdout.write("\rr%d" % rev)
        sys.stdout.flush()

    svnlogparser.load_revs(xmlfile, cursor, progress)

    process.wait()
    print ""

    print "updating unique_paths table..."
    cursor.execute('''
        INSERT INTO unique_paths
        SELECT DISTINCT path
        FROM modified_paths
        WHERE revision >= %s
        AND (
            SELECT COUNT(*)
            FROM unique_paths
            WHERE unique_paths.path=modified_paths.path
        ) = 0
    ''', (last_rev,))
    print "%d new paths added" % cursor.rowcount

    print "clearing revlist cache..."
    cursor.execute("TRUNCATE TABLE cache_entry, cache_revs")

    print "committing transaction..."
    conn.commit()

    print "all done!"
    cursor.close()
    conn.close()

if __name__ == "__main__":
    main()
