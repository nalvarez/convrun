/*
 *  Copyright (C) 2011 Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sshconversionrunner.h"

#include <QTcpSocket>
#include <QStringList>

#include "sshqt.h"

struct SshConversionRunnerPrivate
{
    QTcpSocket m_tcpSocket;
    SshSession& m_session;

    SshConversionRunnerPrivate(SshSession& session);
};
SshConversionRunnerPrivate::SshConversionRunnerPrivate(SshSession& session)
    : m_tcpSocket()
    , m_session(session)
{
}

SshConversionRunner::SshConversionRunner(SshSession& session)
    : d(new SshConversionRunnerPrivate(session))
{
}

SshConversionRunner::~SshConversionRunner()
{
    delete d;
}

QString escapeShellArg(QString str) {
    str.replace('\'', "\'\\\'\'");
    str.prepend('\'');
    str.append('\'');
    return str;
}
QString escapeShellCommand(const QStringList& args) {
    QString command;
    foreach (QString arg, args) {
        command += "'" + arg.replace('\'', "\'\\\'\'") + "' ";
    }
    command.chop(1); // delete the final space
    return command;
}

bool SshConversionRunner::startConversion(const ConversionParams& params)
{
    SshChannel channel(d->m_session);

    QStringList convCmd("svn-all-fast-export");
    if (params.addMetadata) {
        convCmd << "--add-metadata";
    }
    if (params.debugRules) {
        convCmd << "--debug-rules";
    }
    if (!params.identityMap.isEmpty()) {
        convCmd << "--identity-map" << params.identityMap;
    }
    if (!params.revisionsFile.isEmpty()) {
        convCmd << "--revisions-file" << params.revisionsFile;
    }
    convCmd << "--rules" << params.rulesFile;
    convCmd << params.svnRepo;

    QString command;
    if (!params.cwd.isEmpty()) {
        command += QString("cd %1 && ").arg(escapeShellArg(params.cwd));
    }
    command += escapeShellCommand(convCmd) + " 2> stderr";

    //command = "cat nalvarez/bindings/kdenetwork/stdout";

    int rc;
    rc = channel.processStartup(command.toUtf8());
    if (rc != 0) {
        return false;
    }

    QTextStream channelStream(&channel);
    QString line;
    do {
        line = channelStream.readLine();
        if (line.startsWith("Exporting")) {
            const QString strRev = line.split(' ').at(2);
            bool ok=false;
            int rev = strRev.toInt(&ok);
            if (ok) {
                emit progress(rev);
            }
        }
    } while (!line.isNull());

    channel.close();

    return true;
}
