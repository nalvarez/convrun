/*
 *  Copyright (C) 2011 Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SSHCONVERSIONRUNNER_H
#define SSHCONVERSIONRUNNER_H

#include <QString>
#include <QObject>
#include <QDebug>

class QFile;

class SshSession;

struct ConversionParams
{
    QString rulesFile; ///< the rules file that determines what goes where
    bool addMetadata; ///< If true, each git commit will have svn commit info
    bool debugRules; ///< print what rule is being used for each file
    QString identityMap; ///< provide map between svn username and email
    QString svnRepo; ///< Path to subversion repo

    /// Directory on the server where the conversion will be run.
    /// The repository and logs will be stored there.
    QString cwd;
    QString revisionsFile; ///< provide a file with revision numbers that should be processed
};

class SshConversionRunnerPrivate;

class SshConversionRunner: public QObject
{
    Q_OBJECT
public:
    SshConversionRunner(SshSession& session);
    virtual ~SshConversionRunner();
    bool startConversion(const ConversionParams& params);

signals:
    void progress(int revision);

private:
    SshConversionRunnerPrivate* const d;
};

class QDebugProgressReporter: public QObject {
    Q_OBJECT
public slots:
    void reportProgress(int rev) {
        qDebug() << "processing revision" << rev;
    }
};
#include <cstdio>
class StdoutProgressReporter: public QObject {
    Q_OBJECT
public:
    StdoutProgressReporter(): out(stdout) {}
public slots:
    void reportProgress(int rev) {
        out << (QString("\rprocessing revision %1").arg(rev));
        out.flush();
    }
private:
    QTextStream out;
};

#endif // SSHCONVERSIONRUNNER_H
