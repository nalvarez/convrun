/*
 *  Copyright (C) 2012 Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "psqlrevlister.h"

#include <QString>
#include <QList>
#include <QSet>
#include <QRegExp>
#include <QDebug>
#include <QStringList>

#include <pqxx/connection>
#include <pqxx/transaction>

PSqlRevLister::PSqlRevLister(const char* connstr): m_conn(connstr)
{
}

QString esc(const pqxx::basic_transaction& tx, const QString& str) {
    const QByteArray data = str.toAscii();
    return QString::fromStdString(tx.esc(data.data(), data.size()));
}

QString getRegexPrefix(const QString& str) {
    static QRegExp prefixRegex("^[\\w/]*(?!\\?)");
    if (prefixRegex.indexIn(str) < 0) {
        return QString();
    }
    return prefixRegex.cap(0);
}

QString realCreateQuery(const QList<QString>& regexes, const pqxx::basic_transaction& tx);
QList<int> realParseResult(const pqxx::result& result);

QString getUncachedCreateQuery(const QSet<QString>& regexes, const pqxx::basic_transaction& tx);
void getUncachedParseResult(const pqxx::result& result, QSet<QString>* regexes);

QString computeAndCacheCreateQuery(const QString regex, const pqxx::basic_transaction& tx);

QString getCachedRevsCreateQuery(const QSet<QString>& regexes, const pqxx::basic_transaction& tx);
QList<int> getCachedRevsParseResult(const pqxx::result& result);

QList<int> PSqlRevLister::generateRevList(const QList<QString>& regexes)
{
    pqxx::work tx1(m_conn);

    if (esc(tx1, "\\d+\\.") != QString("\\\\d+\\\\.")) {
        qFatal("libpqxx isn't escaping strings correctly; can't continue");
    }

    QSet<QString> uncachedRegexes = regexes.toSet();

    QString queryGetUncached = getUncachedCreateQuery(uncachedRegexes, tx1);
    pqxx::result result = tx1.exec(queryGetUncached.toStdString());
    getUncachedParseResult(result, &uncachedRegexes);
    tx1.commit();
    int count=uncachedRegexes.size();
    int i=0;
    foreach(QString regex, uncachedRegexes) {
        i++;
        pqxx::work tx(m_conn);
        QString queryComputeAndCache = computeAndCacheCreateQuery(regex, tx);
        qDebug() << qPrintable(QString("[%1/%2]").arg(i).arg(count)) << "Getting revisions for" << regex;
        try {
            pqxx::result result = tx.exec(std::string("INSERT INTO cache_entry VALUES(E'") + tx.esc(regex.toStdString()) + "')");
        } catch (pqxx::unique_violation& e) {
            qDebug() << "The regex" << regex << "is already cached, skipping";
            continue;
        }
        pqxx::result result2 = tx.exec(queryComputeAndCache.toStdString());
        tx.commit();
    }

    qDebug() << "Loading all revisions from cache...";
    pqxx::work tx2(m_conn);
    const QString queryGetCachedRevs = getCachedRevsCreateQuery(regexes.toSet(), tx2);
    pqxx::result result2 = tx2.exec(queryGetCachedRevs.toStdString());

    return getCachedRevsParseResult(result2);
}

QString getUncachedCreateQuery(const QSet<QString>& regexes, const pqxx::basic_transaction& tx)
{
    QString cacheQuery = "SELECT regex FROM cache_entry WHERE regex IN (";
    foreach (QString pattern, regexes) {
        cacheQuery += "E'" + esc(tx, pattern) + "', ";
    }
    cacheQuery.resize(cacheQuery.length()-2);
    cacheQuery += ")";

    return cacheQuery;
}
void getUncachedParseResult(const pqxx::result& result, QSet<QString>* regexes)
{
    for (pqxx::result::const_iterator row = result.begin(); row != result.end(); ++row) {
        QString cachedRegex = QString::fromAscii(row[0].c_str());
        qDebug() << "already cached regex:" << cachedRegex;
        if (!regexes->remove(cachedRegex)) {
            qWarning() << "got" << cachedRegex << "which isn't even in the set!";
        }
    }
}
QString computeAndCacheCreateQuery(const QString regex, const pqxx::basic_transaction& tx)
{
    QString regexPrefix = getRegexPrefix(regex);

    // if the regex is something like "foo/" or "foo/$",
    // we need to strip off the / for the LIKE expression,
    // because paths in the database don't ever finish in a slash.
    if (regexPrefix.endsWith('/')) {
        regexPrefix.chop(1);
    }

    QString query = "INSERT INTO cache_revs "
                      "SELECT DISTINCT "
                      "E'%1', "
                      "revision "
                      "FROM modified_paths WHERE path IN ("
                        "SELECT path "
                        "FROM unique_paths "
                        "WHERE path LIKE E'%2%' AND (path ~ E'%3' OR path || '/' ~ E'%3')"
                      ")";
    query = query.arg(esc(tx, regex), esc(tx, regexPrefix), esc(tx, '^'+regex));
    return query;
}

QString getCachedRevsCreateQuery(const QSet< QString >& regexes, const pqxx::basic_transaction& tx)
{
    QString query = "SELECT DISTINCT revision FROM cache_revs WHERE regex IN (";
    foreach (QString pattern, regexes) {
        query += "E'" + esc(tx, pattern) + "', ";
    }
    query.resize(query.length()-2);
    query += ") ORDER BY revision";

    return query;
}
QList<int> getCachedRevsParseResult(const pqxx::result& result)
{
    return realParseResult(result);
}



QString realCreateQuery(const QList<QString>& regexes, const pqxx::basic_transaction& tx)
{
    QStringList regexMatches;

    foreach (QString pattern, regexes) {
        if (pattern.endsWith("/$")) {
            pattern.replace(pattern.size()-2, 2, "/?$");
        }
        if (pattern.endsWith("/")) {
            pattern.replace(pattern.size()-1, 1, "(?:/|$)");
        }
        const QString regexPrefix = getRegexPrefix(pattern);

        QString regexMatch = QString("(path like E'%1%' and path ~ E'^%2')").arg(esc(tx, regexPrefix)).arg(esc(tx, pattern));
        regexMatches.append(regexMatch);
    }

    QString query = QString("select distinct revision from modified_paths where path in (select path from unique_paths where %1) order by revision;")
                    .arg(regexMatches.join(" or "));
    return query;
}

QList<int> realParseResult(const pqxx::result& result)
{
    QList<int> revisions;

    //TODO use Qt's foreach() here
    for (pqxx::result::const_iterator i = result.begin(); i != result.end(); ++i) {
        int rev;
        if (!i[0].to(rev)) throw std::runtime_error("No rev!");
        revisions.append(rev);
    }
    return revisions;
}
int PSqlRevLister::getLastRevision()
{
    pqxx::work tx(m_conn);

    pqxx::result result = tx.exec("SELECT MAX(revision) FROM modified_paths");
    if (result.size() != 1) {
        throw std::runtime_error("getLastRevision query failed");
    }
    int rev;
    if (!result.at(0)[0].to(rev)) {
        throw std::runtime_error("Couldn't parse last rev");
    }
    return rev;
}
