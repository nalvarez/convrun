/*
 *  Copyright (C) 2011 Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SSHQT_H
#define SSHQT_H

#include <QIODevice>

#include <libssh2.h>
#include <libssh2_sftp.h>

class QAbstractSocket;

class SshStaticInit {
public:
    SshStaticInit();
    ~SshStaticInit();
private:
    Q_DISABLE_COPY(SshStaticInit)
};

class SshSession {
public:

    enum HostKeyHashType {
        HashMD5, HashSHA1
    };

    SshSession();
    virtual ~SshSession();
    int startup(int fd);
    int startup(QAbstractSocket& socket);
    QByteArray hostHash(HostKeyHashType hashType) const;
    int authPublicKeyFromFile(const QByteArray& username, const char* publicKeyPath, const char* privateKeyPath, const char* passphrase);

private:
    LIBSSH2_SESSION* m_session;
    friend class SshChannel;
    friend class SshSFTP;
};

class SshChannel: public QIODevice {
public:
    SshChannel(SshSession& session);
    virtual ~SshChannel();

    int processStartup(const QByteArray& command);

    virtual void close();

    virtual qint64 readData(char* data, qint64 maxlen);
    virtual qint64 writeData(const char* data, qint64 len);
    virtual bool isSequential() const;

private:
    LIBSSH2_CHANNEL* m_channel;
};

class SshSFTP {
public:
    SshSFTP(SshSession& session);
    ~SshSFTP();
    int mkdir(const QString& path);

private:
    LIBSSH2_SFTP* m_sftp;
    friend class SftpFileHandle;
};

class SftpFileHandle: public QIODevice {
public:
    SftpFileHandle(SshSFTP& sftp);
    virtual ~SftpFileHandle();
    bool openFile(QString filename, QIODevice::OpenMode openMode, int permissions);
    virtual void close();

    virtual qint64 readData(char* data, qint64 maxlen);
    virtual qint64 writeData(const char* data, qint64 len);

private:
    SshSFTP& m_sftp;
    LIBSSH2_SFTP_HANDLE* m_handle;

};

#endif // SSHQT_H
