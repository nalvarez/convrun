/*
 *  Copyright (C) 2012 Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCoreApplication>
#include <QDebug>
#include <QStringList>

#include "ruleparser.h"
#include "psqlrevlister.h"

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    const QStringList args = app.arguments();

    if (args.size() != 3) {
        qDebug() << "usage: makerevlist <db name> <rules file>";
        return 1;
    }
    const QString dbName = args.at(1);
    const QString inputPath = args.at(2);

    QList<int> revs;
    int lastRev;
    Rules rules(inputPath);
    rules.load();
    {
        QStringList regexes;
        foreach (const Rules::Match rule, rules.matchRules()) {
            if (rule.action == Rules::Match::Ignore) continue;

            regexes.append(rule.rx.pattern());
        }
        QString connString = QString("dbname=%1").arg(dbName);
        PSqlRevLister generator(connString.toStdString().c_str());
        qDebug() << "Generating revlist...";
        revs = generator.generateRevList(regexes);
        lastRev = generator.getLastRevision();
    }

    QTextStream outputStream(stdout, QIODevice::WriteOnly);
    foreach (int rev, revs) {
        outputStream << rev << '\n';
    }
    outputStream << lastRev << "-HEAD" << '\n';
    outputStream.flush();

    return 0;
}
