/*
 *  Copyright (C) 2011 Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sshqt.h"

#include <QAbstractSocket>

#include <libssh2.h>

SshStaticInit::SshStaticInit()
{
    libssh2_init(0);
}
SshStaticInit::~SshStaticInit()
{
    libssh2_exit();
}

SshSession::SshSession()
{
    m_session = libssh2_session_init();
}
SshSession::~SshSession()
{
    if (m_session) {
        libssh2_session_free(m_session);
    }
}

int SshSession::startup(int fd)
{
    return libssh2_session_startup(m_session, fd);
}
int SshSession::startup(QAbstractSocket& socket)
{
    return startup(socket.socketDescriptor());
}

QByteArray SshSession::hostHash(SshSession::HostKeyHashType hashType) const
{
    size_t length = 0;
    int hashtype_libssh = -1;
    switch (hashType) {
    case HashMD5:
        length = 16;
        hashtype_libssh = LIBSSH2_HOSTKEY_HASH_MD5;
        break;
    case HashSHA1:
        length = 20;
        hashtype_libssh = LIBSSH2_HOSTKEY_HASH_SHA1;
        break;
    default:
        return QByteArray();
    }
    const char* data = libssh2_hostkey_hash(m_session, hashtype_libssh);
    return QByteArray(data, length);
}

int SshSession::authPublicKeyFromFile(const QByteArray& username, const char* publicKeyPath, const char* privateKeyPath, const char* passphrase)
{
    int rc = libssh2_userauth_publickey_fromfile_ex(m_session,
                                                    username.constData(), username.length(),
                                                    publicKeyPath, privateKeyPath,
                                                    passphrase);
    return rc;
}


SshChannel::SshChannel(SshSession& session)
{
    m_channel = libssh2_channel_open_session(session.m_session);
}
SshChannel::~SshChannel()
{
    close();
    libssh2_channel_free(m_channel);
}
void SshChannel::close()
{
    if (isOpen()) {
        QIODevice::close();
        libssh2_channel_close(m_channel);
    }
}


int SshChannel::processStartup(const QByteArray& command)
{
    int rc = libssh2_channel_process_startup(m_channel, "exec", 4 /* strlen("exec") */,
                                             command, command.length());
    if (rc == 0) {
        QIODevice::open(ReadWrite);
    }
    return rc;
}


qint64 SshChannel::readData(char* data, qint64 maxlen) {
    return libssh2_channel_read_ex(m_channel, 0, data, maxlen);
}
qint64 SshChannel::writeData(const char* data, qint64 len) {
    return -1;
}
bool SshChannel::isSequential() const {
    return true;
}

SshSFTP::SshSFTP(SshSession& session)
{
    m_sftp = libssh2_sftp_init(session.m_session);
}
SshSFTP::~SshSFTP()
{
    libssh2_sftp_shutdown(m_sftp);
}
int SshSFTP::mkdir(const QString& path)
{
    return libssh2_sftp_mkdir_ex(m_sftp, path.toAscii(), path.toAscii().length(), 0777);
}

SftpFileHandle::SftpFileHandle(SshSFTP& sftp)
    : m_sftp(sftp)
    , m_handle(0)
{
}

bool SftpFileHandle::openFile(QString filename, QIODevice::OpenMode openMode, int permissions)
{
    int flagsForLibssh = 0;
    if (openMode.testFlag(QIODevice::ReadOnly))  flagsForLibssh |= LIBSSH2_FXF_READ;
    if (openMode.testFlag(QIODevice::WriteOnly)) flagsForLibssh |= LIBSSH2_FXF_WRITE|LIBSSH2_FXF_CREAT;
    if (openMode.testFlag(QIODevice::Append))    flagsForLibssh |= LIBSSH2_FXF_APPEND;
    if (openMode.testFlag(QIODevice::Truncate))  flagsForLibssh |= LIBSSH2_FXF_TRUNC;

    LIBSSH2_SFTP_HANDLE* handle = libssh2_sftp_open_ex(m_sftp.m_sftp,
                                                       filename.toAscii(), filename.toAscii().length(),
                                                       flagsForLibssh, permissions, LIBSSH2_SFTP_OPENFILE);
    if (!handle) {
        return false;
    } else {
        this->m_handle = handle;
        QIODevice::open(openMode);
        return true;
    }
}
qint64 SftpFileHandle::writeData(const char* data, qint64 len)
{
    return libssh2_sftp_write(m_handle, data, len);
}
qint64 SftpFileHandle::readData(char* data, qint64 maxlen)
{
    return libssh2_sftp_read(m_handle, data, maxlen);
}
void SftpFileHandle::close()
{
    if (m_handle) {
        libssh2_sftp_close_handle(m_handle);
        m_handle = 0;
    }
    QIODevice::close();
}
SftpFileHandle::~SftpFileHandle()
{
    close();
}
