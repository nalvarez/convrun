/*
 *  Copyright (C) 2011 Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCoreApplication>
#include <QDebug>
#include <QTcpSocket>
#include <QFile>
#include <QStringList>
#include <QDateTime>
#include <QDir>

#include "sshconversionrunner.h"
#include "sshqt.h"
#include "ruleparser.h"
#include "psqlrevlister.h"

const char dewey_hostkey[16] = {0x33,0x6d,0x9f,0xcd,0xb0,0x4c,0x36,0xb9,0x0f,0x34,0x93,0x30,0x49,0x00,0xae,0x88};
const char louie_hostkey[16] = {0x04,0x0b,0xf3,0x90,0xee,0x06,0xd3,0x60,0x8b,0xb8,0xc5,0x93,0x61,0x24,0x8a,0x2d};

bool uploadFile(const QString& localPath, SshSFTP& sftp, const QString& remotePath) {
    SftpFileHandle handle(sftp);
    handle.openFile(remotePath, QIODevice::WriteOnly|QIODevice::Truncate, 0644);

    QFile file(localPath);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "failed to open file";
        return false;
    }
    int bytesRead = 0, totalWritten = 0;
    char buf[16384];
    forever {
        bytesRead = file.read(buf, sizeof(buf));
        if (bytesRead <= 0) break;
        totalWritten=0;
        while (totalWritten < bytesRead) {
            int writtenNow = handle.write(buf + totalWritten, bytesRead-totalWritten);
            if (writtenNow < 0) {
                handle.close();
                return false;
            }
            totalWritten += writtenNow;
        }
    }
    handle.close();
    file.close();
    return true;
}

QString commonPrefixPath(QStringList paths) {
    paths.sort();

    QString path1 = paths.first();
    QString path2 = paths.last();
    QStringList comp1 = path1.split('/');
    QStringList comp2 = path2.split('/');

    // The -1 is to make sure the last component (usually a filename) isn't touched.
    // This mainly causes problems if path1==path2 (paths.length()==1);
    // as the common prefix of ["a/b", "a/b"] should be "a", not "a/b".
    int minComponents = qMin(comp1.length(), comp2.length()) - 1;
    int matchingComponents = minComponents;

    for (int i=0; i<minComponents; ++i) {
        if (comp1[i] != comp2[i]) {
            matchingComponents = i;
            break;
        }
    }
    comp1.erase(comp1.begin() + matchingComponents, comp1.end());
    return comp1.join("/");
}

int main(int argc, char* argv[])
{
    qFatal("This code is unmaintained!");
    QCoreApplication app(argc, argv);

    const QStringList args = app.arguments();

    if (args.size() < 2) {
        qDebug() << "usage: convrun <rules file> [<server>]";
        qDebug() << "<server> can be 'dewey' or 'louie' (defaults to 'dewey')";
        return 1;
    }
    const QString inputPath = QDir(args.at(1)).absolutePath();

    QString serverName = "dewey"; // default
    if (args.size() == 3) {
        serverName = args.at(2);
    }
    QString hostname;
    QByteArray expectedHostHash;
    quint16 portNumber = 22;
    if (serverName == "dewey") {
        hostname = "dewey.kde.org";
        expectedHostHash = QByteArray(dewey_hostkey, 16);
    } else if (serverName == "louie") {
        hostname = "louie.kde.org";
        portNumber = 22308;
        expectedHostHash = QByteArray(louie_hostkey, 16);
    } else {
        qDebug() << "unknown server";
        return 1;
    }

    SshStaticInit sshInit;

    SshSession session;

    QTcpSocket tcpSocket;
    tcpSocket.connectToHost(hostname, portNumber);
    if (tcpSocket.waitForConnected() == false) {
        qDebug() << "connection failed;" << tcpSocket.errorString();
        return 1;
    }
    if (session.startup(tcpSocket.socketDescriptor()) != 0) {
        qDebug() << "couldn't establish ssh connection";
        tcpSocket.close();
        return 1;
    }

    {
        QByteArray actualHostHash(session.hostHash(SshSession::HashMD5));
        if (actualHostHash != expectedHostHash) {
            qDebug() << "host key invalid";
            tcpSocket.close();
            return 2;
        }
    }

    Q_ASSERT(session.authPublicKeyFromFile("git", "/home/nicolas/.ssh/dvcs_dsa.pub", "/home/nicolas/.ssh/dvcs_dsa", NULL) == 0);

    QList<int> revs;
    Rules rules(inputPath);
    rules.load();
    {

        QStringList regexes;
        foreach (const Rules::Match rule, rules.matchRules()) {
            if (rule.action == Rules::Match::Ignore) continue;

            regexes.append(rule.rx.pattern());
        }
        PSqlRevLister generator("");
        qDebug() << "Generating revlist...";
        revs = generator.generateRevList(regexes);
    }
    QString repoName = rules.repositories().first().name;
    repoName.replace('/','_');
    QString serverPath = QString("nalvarez/conv-%1-%2").arg(repoName, QDateTime::currentDateTime().toString("MMdd-hhmmss"));

    QString serverRootRulePath;
    {
        qDebug() << "Connecting to SFTP...";
        SshSFTP sftp(session);
        sftp.mkdir(serverPath);

        QStringList localPaths;
        localPaths << inputPath;
        foreach (const QString included, rules.includedFiles()) {
            localPaths << QDir::cleanPath(included);
        }

        QString commonPrefix = commonPrefixPath(localPaths);
        serverRootRulePath = inputPath.mid(commonPrefix.length()+1);

        QSet<QString> createdDirs;

        foreach (const QString localPath, localPaths) {
            QString relPath = localPath.mid(commonPrefix.length()+1);
            QStringList components = relPath.split('/');
            for (int i=0; i<components.length()-1; ++i) {
                QString dirpath = QStringList(components.mid(0, i+1)).join("/");
                if (!createdDirs.contains(dirpath)) {
                    qDebug() << "creating directory" << dirpath;
                    sftp.mkdir(serverPath + "/" + dirpath);
                    createdDirs.insert(dirpath);
                }
            }
            qDebug() << qPrintable(QString("Uploading %1...").arg(relPath));
            uploadFile(localPath, sftp, serverPath + "/" + relPath);
        }

        qDebug() << "Uploading revlist...";
        SftpFileHandle revListFile(sftp);
        revListFile.openFile(serverPath + "/revlist", QIODevice::WriteOnly|QIODevice::Truncate, 0644);

        QTextStream outputStream(&revListFile);
        foreach (int rev, revs) {
            outputStream << rev << '\n';
        }
        qDebug() << "done";
    }

    {
    SshConversionRunner runner(session);

    ConversionParams params;
    params.identityMap = "/home/gitmaster/kde-ruleset/account-map";
    params.svnRepo = "/home/gitmaster/svn/";
    params.addMetadata = true;
    params.debugRules = true;
    params.rulesFile = serverRootRulePath;
    params.revisionsFile = "revlist";
    params.cwd = serverPath;

    StdoutProgressReporter reporter;
    QObject::connect(&runner, SIGNAL(progress(int)), &reporter, SLOT(reportProgress(int)));
    qDebug() << "running conversion...";
    Q_ASSERT(runner.startConversion(params));
    }

    qDebug() << qPrintable(QString("conversion done; %1:%2").arg(hostname, serverPath));

    return 0;
}
