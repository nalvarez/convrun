/*
 *  Copyright (C) 2012 Nicolas Alvarez <nicolas.alvarez@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PSQLREVLISTER_H
#define PSQLREVLISTER_H

#include <QList>
#include <QString>
#include <pqxx/connection>

class PSqlRevLister
{
public:
    PSqlRevLister(const char* connstr);
    QList<int> generateRevList(const QList<QString>& regexes);
    int getLastRevision();
private:
    pqxx::connection m_conn;
};

#endif // PSQLREVLISTER_H
