#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
This script parses a XML log from "svn log --xml" and inserts it into a
PostgreSQL database.
"""

from xml.etree import cElementTree as ElementTree
import psycopg2

def load_revs(xmlfile, cursor, progress_cb=None):
    """
    Loads revisions from the SVN XML log 'xmlfile' (a file-like object),
    and inserts them into the modified_paths table using the specified cursor.

    progress_cb is a function that will be called for every processed revision,
    passing the revision number as only argument.

    Note this function will not commit the transaction.
    """
    for event,elem in ElementTree.iterparse(xmlfile):
        if elem.tag == "logentry":
            rev = int(elem.get("revision"))
            rows=[]
            for pathelem in elem.findall("paths/path"):
                rows.append((
                    rev,
                    pathelem.text.encode("utf8"),
                    pathelem.get("action"),
                ))
                pathelem.clear()

            cursor.executemany("INSERT INTO modified_paths VALUES(%s,%s,%s)", rows)

            if progress_cb:
                progress_cb(rev)

            elem.clear()

def main():
    import sys
    xmlfile = sys.stdin
    conn = psycopg2.connect(database="nicolas")
    c = conn.cursor()

    def progress(rev):
        sys.stdout.write("\rr%d" % rev)
        sys.stdout.flush()

    load_revs(xmlfile, c, progress)

    sys.stdout.write("\ncommitting...")
    sys.stdout.flush()
    conn.commit()
    print " done"

if __name__ == "__main__":
    main()
